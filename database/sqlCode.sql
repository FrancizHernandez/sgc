create database asocoop7;

use asocoop7;

select * from	users;
CREATE TABLE users (
id INT NOT NULL AUTO_INCREMENT,
fullname VARCHAR(100) NOT NULL,
username VARCHAR(45) NOT NULL,
passwd VARCHAR(45) NOT NULL,
role ENUM('R','S') NOT NULL,
blocked ENUM('N','Y') NOT NULL,
PRIMARY KEY (id));


ALTER TABLE users ADD blocked ENUM('N','Y') NOT NULL;

CREATE TABLE estado_afiliado (
id INT NOT NULL AUTO_INCREMENT,
descripcion VARCHAR(200) NOT NULL,
PRIMARY KEY (id)
);

select * from estado_afiliado;
CREATE TABLE laboral (
id INT NOT NULL AUTO_INCREMENT,
id_afiliado INT NOT NULL,
ciudad VARCHAR(100) NOT NULL,
departamento VARCHAR(200) NOT NULL,
puesto VARCHAR(45) NOT NULL,
salario_bruto  DOUBLE NOT NULL,
salario_liquido DOUBLE NOT NULL,
telefono VARCHAR(45) NOT NULL,
extension VARCHAR(45) NOT NULL,
correo VARCHAR(100) NOT NULL, 
PRIMARY KEY (id));
ALTER TABLE laboral ADD CONSTRAINT FK_empresa_afiliaso FOREIGN KEY (id_afiliado) REFERENCES afiliado(id);

CREATE TABLE afiliado (
id INT NOT NULL AUTO_INCREMENT,
cedula VARCHAR(100) NOT NULL,
nombre VARCHAR(100) NOT NULL,
direccion VARCHAR(200) NOT NULL,
telefono  VARCHAR(40),
celular  VARCHAR(40),
correo VARCHAR(45) NOT NULL,
fecha_ingreso DATE,
sesion_ingreso VARCHAR(100) NOT NULL,
fecha_salida DATE,
sesion_salida VARCHAR(100) NOT NULL,
fecha_nac DATE, 
genero ENUM('F','M') NOT NULL,
estado_civil VARCHAR(45) NOT NULL,
estado INT NOT NULL, -- * --
PRIMARY KEY (id));
select * from afiliado;
	

ALTER TABLE afiliado ADD CONSTRAINT FK_afiliado_estado FOREIGN KEY (estado) REFERENCES estado_afiliado(id);


CREATE TABLE beneficiario (
id INT NOT NULL AUTO_INCREMENT,
id_afiliado INT NOT NULL, -- * --
cedula VARCHAR(100) NOT NULL,
nombre VARCHAR(100) NOT NULL,
parentesco VARCHAR(100) NOT NULL,
direccion VARCHAR(200) NOT NULL,
telefono  VARCHAR(45),
celular  VARCHAR(45),
cedula_albacea VARCHAR(45),
nombre_albacea VARCHAR(100),
telefono_albacea  VARCHAR(50),
direccion_albacea  VARCHAR(100),
PRIMARY KEY (id));
ALTER TABLE beneficiario ADD CONSTRAINT FK_beneficiario_afiliado FOREIGN KEY (id_afiliado) REFERENCES afiliado(id);

-- CREDITO --
CREATE TABLE garantia (
id INT NOT NULL AUTO_INCREMENT,
descripcion VARCHAR(100) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE linea_credito (
id INT NOT NULL AUTO_INCREMENT,
codigo VARCHAR(100) NOT NULL,
descripcion VARCHAR(100) NOT NULL,
tasa_interes DOUBLE NOT NULL,
PRIMARY KEY (id));

CREATE TABLE op_credito (
id INT NOT NULL AUTO_INCREMENT,
consecutivo VARCHAR(100) NOT NULL,
afiliado INT NOT NULL, -- *--
id_linea INT NOT NULL, -- *--
principal DOUBLE NOT NULL,
fecha_formalizacion DATE,
plazo INT NOT NULL,
fecha_inicio_pago DATE,
fecha_fin_pago DATE,
periodicidad  ENUM('M','Q','S') NOT NULL,
id_garantia int NOT NULL, -- *--
saldo_ant DOUBLE NOT NULL,
saldo_act DOUBLE NOT NULL, 
PRIMARY KEY (id));


ALTER TABLE op_credito ADD CONSTRAINT FK_credito_afiliado FOREIGN KEY (afiliado) REFERENCES afiliado(id);
ALTER TABLE op_credito ADD CONSTRAINT FK_credito_linea FOREIGN KEY (id_linea) REFERENCES linea_credito(id);
ALTER TABLE op_credito ADD CONSTRAINT FK_credito_garantia FOREIGN KEY (id_garantia) REFERENCES garantia (id);

CREATE TABLE historico_creditos (
id INT NOT NULL AUTO_INCREMENT,
consecutivo VARCHAR(100) NOT NULL,
id_op_credito INT NOT NULL, -- * --- 
cantidad_cuotas INT NOT NULL,
principal DOUBLE NOT NULL,
cuota DOUBLE NOT NULL,
amortizacion DOUBLE NOT NULL,
intereses DOUBLE NOT NULL,
moratorios DOUBLE NOT NULL,
fecha_pago DATE, 
saldo_ant DOUBLE NOT NULL,
saldo_act DOUBLE NOT NULL,
fecha_prox_pago DATE,
fecha_intereses DATE,
PRIMARY KEY (id));

ALTER TABLE historico_creditos ADD CONSTRAINT FK_historial_credito FOREIGN KEY (id_op_credito) REFERENCES op_credito (id);


-- AHORRO --
CREATE TABLE linea_ahorro(
id INT NOT NULL AUTO_INCREMENT,
codigo VARCHAR(100) NOT NULL,
descripcion VARCHAR(100) NOT NULL,
tasa_interes DOUBLE NOT NULL,
PRIMARY KEY (id));

CREATE TABLE op_ahorro (
id INT NOT NULL AUTO_INCREMENT,
consecutivo VARCHAR(100) NOT NULL,
afiliado INT NOT NULL, -- * --
linea_ahorro INT NOT NULL, -- * --
monto DOUBLE NOT NULL,
plazo INT NOT NULL,
fecha_inicio DATE,
fecha_fin DATE,
saldo_ant DOUBLE NOT NULL,
saldo_act DOUBLE NOT NULL, 
PRIMARY KEY (id));

ALTER TABLE op_ahorro ADD CONSTRAINT FK_ahorro_afiliado FOREIGN KEY (afiliado) REFERENCES afiliado(id);
ALTER TABLE op_ahorro ADD CONSTRAINT FK_ahorro_linea FOREIGN KEY (linea_ahorro) REFERENCES linea_ahorro(id);

CREATE TABLE historico_ahorro (
id INT NOT NULL AUTO_INCREMENT,
consecutivo VARCHAR(100) NOT NULL,
id_linea INT NOT NULL, -- * --- 
monto DOUBLE NOT NULL,
interes_ganado DOUBLE NOT NULL,
fecha_ahorro DATE, 
saldo_ant DOUBLE NOT NULL,
saldo_act DOUBLE NOT NULL,
fecha_prox_pago DATE,
fecha_intereses DATE,
PRIMARY KEY (id));

ALTER TABLE historico_ahorro ADD CONSTRAINT FK_historial_ahorro FOREIGN KEY (id_linea) REFERENCES linea_ahorro(id);

select *from afiliado;
ALTER TABLE users DROP COLUMN blocked;
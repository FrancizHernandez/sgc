<?php
return [
    /*
    |------------------------------------------------------
    | Parámetros generales
    |------------------------------------------------------
    | Definir los parámetros generales de la aplicación
    */
    'debug' => true,
    'public' => "/",
    /*
    |------------------------------------------------------
    | Parámetros de Base de datos
    |------------------------------------------------------
    | En esta sección se definen los parámetros de conexión a DB
    */
    'server' => 'localhost',
    'database' => 'asocoop7',
    'user' => 'isw613_user',
    'password' => 'secret',
];

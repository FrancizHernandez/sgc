<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';


?>
<div class="container">
    <div class="container">
        <br>
        <h1>Mantenimiento de Respuestas</h1>
        <div class="form-group">
            <label for="username">Cuestionario:</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="">
        </div>
        <div class="form-group">
            <label for="username">Pregunta:</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="">
        </div>
        <table class="table table-hover">
            <thead>
                <tr class="table-success">
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">RESPUESTA</th>
                    <th scope="col">PUNTOS</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($answerCollection as $record) : ?>
                    <tr>
                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#view" data-id="<?= $record["id"] ?>" data-question="<?= $record["question_id"] ?>" data-number="<?= $record["number"] ?>" data-text="<?= $record["answer_text"] ?>" data-points="<?= $record["answer_points"] ?>" data-questionaire="<?= $record["description"] ?>" data-qtext="<?= $record["question_text"] ?>" href="#">Ver</a></td>
                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#edit" data-id="<?= $record["id"] ?>" data-question="<?= $record["question_id"] ?>" data-number="<?= $record["number"] ?>" data-text="<?= $record["answer_text"] ?>" data-points="<?= $record["answer_points"] ?>" data-questionaire="<?= $record["description"] ?>" data-qtext="<?= $record["question_text"] ?>" href="#">Editar</a></td>
                        <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="<?= $record["id"] ?>" data-question="<?= $record["question_id"] ?>" data-number="<?= $record["number"] ?>" data-text="<?= $record["answer_text"] ?>" data-points="<?= $record["answer_points"] ?>" href="#">Eliminar</button></td>
                        <td id="description"><?= $record["answer_text"] ?></td>
                        <td id="description"><?= $record["answer_points"] ?></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
        <div>
            <?php if (count($answerCollection) == 0) : ?>
                <h4 class="text-danger">Pregunta aún sin respuestas!!! Agrega algunas</h4>
            <?php endif; ?>
        </div>
        <a class="btn btn-success" data-toggle="modal" data-target="#new" data-qid=" <?= $record["question_id"] ?>" data-cuestionario="<?= $record["description"] ?>" data-question="<?= $record["question_id"] ?>" data-text="<?= $record["answer_text"] ?>" data-pregunta="<?= $record["question_text"] ?>" href="#">Agregar Respuesta</a>
        <a class="btn btn-primary" href="<?= "/questions/index.php?action=index&id=" . $record['qid']; ?>">Regresar a lista de preguntas</a>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>



<!-- POP UP ELIMINAR -->
<!-- Modal -->

<div class="modal" id="delete">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">ELIMINAR RESPUESTA</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <h5>Está seguro de eliminar la siguiente respuesta:</h5>
                        <input style="display:none" disabled type="text" class="form-control" id="delete_id">
                        <input style="display:none" disabled type="text" class="form-control" id="delete_idQuestion">
                        <input disabled type="text" class="form-control" id="delete_respuesta">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger" aria-hidden="true" href="" id="delete_btn" onclick="deleteQuestion();">Eliminar</a>
            </div>
        </div>
    </div>
</div>

<!-- POP UP ver -->
<!-- Modal -->
<div class="modal" id="view">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">VER RESPUESTA</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset disabled>
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="view_id">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input type="text" class="form-control" id="view_cuestionario">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Pregunta:</label>
                        <input type="text" class="form-control" id="view_pregunta">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Respuesta:</label>
                        <input type="text" class="form-control" id="view_respuesta">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Puntos:</label>
                        <input type="text" class="form-control" id="view_puntos">
                    </div>
                </fieldset>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ver reporte</button>
                <a class="btn btn-danger" data-toggle="modal" data-target="#edit" aria-hidden="true">Editar</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar a la lista</button>
            </div>
        </div>
    </div>
</div>
<!-- POP UP edit -->
<!-- Modal -->
<div class="modal" id="edit">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EDITAR RESPUESTA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="index.php?action=update" method="POST">
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="edit_id" name="edit_id">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input disabled type="text" class="form-control" id="edit_cuestionario">

                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Pregunta:</label>
                        <input disabled type="text" class="form-control" id="edit_pregunta" name="edit_pregunta">
                        <input style="display:none" type="text" class="form-control" id="edit_idpreg" name="edit_idpreg">

                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Respuesta:</label>
                        <input type="text" class="form-control" id="edit_respuesta" name="edit_respuesta">
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Puntos</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_ptos" name="edit_ptos">
                        </div>
                    </div>
                    <p class="text-danger" id="new_message"></p>
                    <br>
                    <button type="submit" class="btn btn-danger" aria-hidden="true">Guardar cambios</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="new">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NUEVA RESPUESTA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row">
            </div>
            <div class="modal-body">
                <form action="index.php?action=store" method="POST" name="form_answers">
                    <div class="form-group">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input disabled type="text" class="form-control" id="cuestionario" name="cuestionario">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Pregunta:</label>
                        <input style="display:none" type="text" class="form-control" id="pregId" name="pregId">
                        <input disabled type="text" class="form-control" id="text">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Respuesta:</label>
                        <input type="text" class="form-control" id="respuesta" name="respuesta">
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Puntos</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="ptos" name="ptos">
                        </div>
                    </div>
                    <p class="text-danger" id="new_message"></p>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>

            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</a>
            </div>
        </div>
    </div>
</div>


<?php include VIEWS . '/partials/footer.php' ?>
<script>
    $('#delete').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#delete_id').val(info.data('id'));
        $('#delete_idQuestion').val(info.data('question'));
        $('#delete_respuesta').val(info.data('text'));

        $('#delete_btn').attr("href", "index.php?action=delete&id=" + info.data('id') + '$quest=') + info.data('questId');
    })

    $('#edit').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#edit_id').val(info.data('id'));
        $('#edit_cuestionario').val(info.data('questionaire'));
        $('#edit_pregunta').val(info.data('qtext'));
        $('#edit_respuesta').val(info.data('text'));
        $('#edit_ptos').val(info.data('points'));
        $('#edit_idpreg').val(info.data('question'));
    })

    $('#new').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#cuestionario').val(info.data('cuestionario'));
        //info pregunta
        $('#pregId').val(info.data('qid'));
        $('#text').val(info.data('pregunta'));
    })

    $('#view').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#view_id').val(info.data('id'));
        $('#view_cuestionario').val(info.data('questionaire'));
        $('#view_pregunta').val(info.data('qtext'));
        $('#view_respuesta').val(info.data('text'));
        $('#view_puntos').val(info.data('points'));
    })



    function updateAnswer() {
        //mandar a guardar datos
        var id = $('#edit_id').val();
        var resp = $('#edit_respuesta').val();
        var puntos = $('#edit_ptos').val();
        var pregId = $('#edit_idpreg').val();
        $('#edit_btn').attr("href", "index.php?action=update&id=" + id + '&preg=' + pregId + '&resp=' + resp + '&pts=' + puntos);
        $('#edit_pregunta').val("");
        $('#edit_ptos').val("");
    }

    function deleteQuestion() {
        //mandar a guardar datos
        var id = $('#delete_id').val();
        var preg = $('#delete_idQuestion').val();
        $('#delete_btn').attr("href", "index.php?action=delete&id=" + id + '&preg=' + preg);
    }
</script>
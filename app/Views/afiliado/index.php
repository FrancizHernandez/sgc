<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';
include VIEWS . '/partials/pillsAsociado.php';
?>
<div class="col-10">
    <div class="container">
        <br>
        <h3 class="text-muted">Afiliados</h3>
        <br>
        <div class="row">
            <div class="col-10">
                <div class="input-group mb-3">
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    <div class="input-group-append  col-sm-8">
                        <button type="button" class="btn btn-info">Buscar</button></div>
                </div>
            </div>
            <div class="col">
                <a href="/afiliados/index.php?action=create" style="text-decoration-color: orangered;">
                    <i class="fas fa-user-plus fa-2x"></i>
                </a>
                <a href="/index.php" style="text-decoration-color: orangered;">
                    <i class="fas fa-sign-out-alt fa-2x"></i>
                </a>
            </div>
        </div>
        <br>

        <table id="users" class="table table-hover">
            <thead>
                <tr class="table-info">
                    <th scope="col"></th>
                    <th scope="col">Cédula</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Estado</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($afiliadoCollection as $record) : ?>
                    <tr>
                        <td><a class="far fa-eye fa-2x" data-toggle="modal" data-target="#view" data-id="<?= $record["id"] ?>" data-description="<?= $record["descripcion"] ?>" href="#"></td>
                        <th scope="row" style="display:none" id="id"><?= $record["id"] ?></th>
                        <td id="fullname" data-id="<?= $record["cedula"] ?>"><?= $record["cedula"] ?></td>
                        <td id="nombre"><?= $record["nombre"] ?></td>
                        <td><?php if ($record["estado"] == "1") {
                                echo "Activo";
                            } else {
                                echo "Renunciado";
                            }
                            ?></td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>
<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';
include VIEWS . '/partials/pillsAsociado.php';
?>
<div class="col-10">
    <div class="container">
        <br>
        <h3 class="text-muted">Afiliados</h3>
        <br>


        <form>


            <div class="form-row align-items-center">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Cédula:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Nombre:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <label for="inputPassword3" class="col-sm-2 col-form-label">Género:</label>
                <div class="col-sm">
                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                        <option selected>...</option>
                        <option value="F">Femenino</option>
                        <option value="M">Masculino</option>
                    </select>
                </div>
                <label for="inputPassword3" class="col-sm-2 col-form-label">Estado Civil:</label>
                <div class="col-sm">
                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                        <option selected>...</option>
                        <option value="S">Soltero (a)</option>
                        <option value="C">Casado (a)</option>
                        <option value="D">Divorciado (a)</option>
                        <option value="V">Viudo (a)</option>
                        <option value="UL">Unión Libre</option>
                    </select>
                </div>
                <div class="col-auto my-1">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Teléfono:</label>
                        <input type="text" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <div class="col-auto my-1">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Celular:</label>
                        <input type="text" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <div class="col-auto my-1">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Correo electrónico:</label>
                        <input type="text" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword6">Password</label>
                    <input type="password" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
                    <small id="passwordHelpInline" class="text-muted">
                        Must be 8-20 characters long.
                    </small>
                </div>
                <div class="col-auto my-1">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>
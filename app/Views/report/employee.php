<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';
?>
<div class="container">
    <div class="container">
        <br>
        <h1>Reporte por Empleado</h1>
        <div class="form-group">
            <label for="username">Empleado:</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="">
        </div>
        <table class="table table-hover">
            <thead>
                <tr class="table-success">
                    <th scope="col">Cuestionario</th>
                    <th scope="col">Puntos</th>
                    <th scope="col">Resultados</th>
                    <th scope="col">Borrar intento</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $results['description'] ?></td>
                    <td><?php echo $results['description'] ?></td>
                    <td><a class="btn btn-primary" href="#">Ver</a></td>
                    <td><a class="btn btn-primary" href="#">X</a></td>

                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>
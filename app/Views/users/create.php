<?php include VIEWS . '/partials/header.php' ?>
<?php include VIEWS . '/partials/navbar.php' ?>
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php include VIEWS . '/partials/message.php' ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <!-- Inicia el formulario de Login -->
            <div class="container">
                <div class="row">
                    <div class="col-10">
                        <h1>Nuevo usuario</h1>
                    </div>
                    <div class="col">
                        <a href="/users/index.php?action=index" style="text-decoration-color: orangered;">
                            <i class="fas fa-sign-out-alt fa-2x"></i>
                        </a>
                    </div>
                </div>
                <br>
                <form action="/users/index.php?action=store" method="post">
                    <div class="form-group row">
                        <label for="fullname" class="col-sm-4 col-form-label">Nombre completo:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="fullname" name="fullname" aria-describedby="Introduzca su nombre del usuario" placeholder="" value=<?= isset($fullname) ? $fullname : "" ?>>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="username" class="col-sm-4 col-form-label">Usuario:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="" value=<?= isset($username) ? $username : "" ?>>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-4 col-form-label">Contraseña:</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password" placeholder="" value=<?= isset($password) ? $password : "" ?>>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="confirm_password" class="col-sm-4 col-form-label">Confirme contraseña:</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="" value=<?= isset($confirm_password) ? $confirm_password : "" ?>>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="role" class="col-sm-4 col-form-label">Rol:</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="role" id="role">
                                <option value="R" <?= isset($role) && ($role == "R") ? "selected" : "" ?>>Regular</option>
                                <option value="S" <?= isset($role) && ($role == "S") ? "selected" : "" ?>>Superusuario</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a class="btn btn-danger" href="/authenticate/index.php?action=store">Cancelar</a>
                </form>
            </div>
        </div>
    </div>
    <?php include VIEWS . '/partials/footer.php' ?>
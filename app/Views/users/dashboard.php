<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';
?>
<div class="container">
    <br>
    <div class="jumboton">
        <?php if (is_null($login)) : ?>
            <p><a href="/authenticate/index.php?action=login" class="btn btn-primary btn-lg" role="button">Iniciar Sesión</a></p>
        <?php else : ?>
            <div id="page-wrapper">
                <div id="page-inner">
                    <!-- /. ROW  -->
                    <div class="row text-center pad-top">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="div-square">
                                <a href="blank.html" class="text-muted" style=" text-decoration-color: orangered;">
                                    <i class="fas fa-cogs fa-5x" style="color:tomato"></i>
                                    <h4 class="text-muted">Seguridad</h4>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="div-square">
                                <a href="blank.html" style="text-decoration-color: orangered;">
                                    <i class="fas fa-users fa-5x" style="color:tomato"></i>
                                    <h4 class="text-muted">Afiliados</h4>
                                </a>
                            </div>


                        </div>


                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="div-square">
                                <a href="blank.html" style="text-decoration-color: orangered;">
                                    <i class="fas fa-piggy-bank  fa-5x" style="color:tomato"></i>
                                    <h4 class="text-muted">Ahorros</h4>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="div-square">
                                <a href="blank.html" style="text-decoration-color: orangered;">
                                    <i class="fas fa-money-check-alt  fa-5x" style="color:tomato"></i>
                                    <h4 class="text-muted">Crédito</h4>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="div-square">
                                <a href="blank.html" style="text-decoration-color: orangered;">
                                    <i class="fas fa-file-invoice   fa-5x " style="color:tomato"></i>
                                    <h4 class="text-muted">Analsis de crédito</h4>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /. PAGE INNER  -->
            </div>
        <?php endif; ?>
    </div>
</div>

<?php include VIEWS . '/partials/footer.php' ?>
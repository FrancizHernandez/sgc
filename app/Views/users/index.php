<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';
?>

<div class="row">
  <div class="col-2">
    <nav>
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Usuarios</a>
        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
      </div>
    </nav>
  </div>

  <div class="col-10">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <br>

            <div class="container">
              <div class="row">
                <div class="col-10">
                  <h3 class="text-muted">Usuarios</h3>
                </div>
                <div class="col">
                  <a href="/index.php" style="text-decoration-color: orangered;">
                    <i class="fas fa-sign-out-alt fa-2x"></i>
                  </a>
                  <a href="/users/index.php?action=create" style="text-decoration-color: orangered;">
                    <i class="fas fa-user-plus fa-2x"></i>
                  </a>
                </div>
              </div>
              <br>

              <table id="users" class="table table-hover">
                <thead>
                  <tr class="table-info">
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">Nombre Completo</th>
                    <th scope="col">Usuario</th>
                    <th scope="col">Rol</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($collection as $record) : ?>
                    <tr>
                      <td><a class="btn btn-primary" data-toggle="modal" data-target="#view" data-id="<?= $record["id"] ?>" data-name="<?= $record["fullname"] ?>" data-user="<?= $record["username"] ?>" data-role="<?= $record["role"] ?>" data-state="<?= $record["blocked"] ?>" href="#">Ver</a></td>
                      <td><a class="btn btn-warning" data-toggle="modal" data-target="#edit" data-id="<?= $record["id"] ?>" data-name="<?= $record["fullname"] ?>" data-user="<?= $record["username"] ?>" data-pass="<?= $record["passwd"] ?>" data-role="<?= $record["role"] ?>" data-state="<?= $record["blocked"] ?>" href="#">Editar</a></td>
                      <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="<?= $record["id"] ?>" data-name="<?= $record["fullname"] ?>" href="#">Eliminar</button></td>
                      <th scope="row" style="display:none" id="id"><?= $record["id"] ?></th>
                      <td id="fullname" data-id="<?= $record["fullname"] ?>"><?= $record["fullname"] ?></td>
                      <td id="username"><?= $record["username"] ?></td>
                      <td><?php if ($record["role"] == "S") {
                            echo "Superusuario";
                          } else {
                            echo "Regular";
                          }
                          ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...</div>
        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
      </div>
    </div>
  </div>




  <!-- POP UP ELIMINAR -->
  <!-- Modal -->

  <div class="modal" id="delete">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <b>
            <h4 class="modal-title">ELIMINAR USUARIO</h4>
          </b>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <h5>Está seguro de eliminar el usuario?</h5>
              <input style="display:none" disabled type="text" class="form-control" id="delete_id">
              <input disabled type="text" class="form-control" id="delete_name">
            </div>
          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-danger" aria-hidden="true" href="" id="delete_btn">Eliminar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- POP UP ver -->
  <!-- Modal -->
  <div class="modal" id="view">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <b>
            <h4 class="modal-title">VER USUARIO</h4>
          </b>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <fieldset disabled>
            <div class="form-group">
              <input style="display:none" type="text" class="form-control" id="view_id">
              <label for="fullname" class="col-form-label">Nombre:</label>
              <input type="text" class="form-control" id="view_fullname">
            </div>
            <div class="form-group">
              <label for="username" class="col-form-label">Usuario:</label>
              <input type="text" class="form-control" id="view_username">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Rol:</label>
              <input type="text" class="form-control" id="view_role">
            </div>
            <div class="form-group">
              <label for="state" class="col-form-label">Estado:</label>
              <input type="text" class="form-control" id="view_state">
            </div>
          </fieldset>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Ver reporte</button>
          <a class="btn btn-danger" data-toggle="modal" data-target="#edit" aria-hidden="true">Editar</a>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar a la lista</button>
        </div>
      </div>
    </div>
  </div>
  <!-- POP UP edit -->
  <!-- Modal -->
  <div class="modal" id="edit">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">EDITAR USUARIO</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="index.php?action=update" method="POST">
            <div class="form-group">
              <input style="display:none" type="text" class="form-control" id="edit_id" name="edit_id">
              <label for="fullname" class="col-form-label">Nombre:</label>
              <input type="text" class="form-control" id="edit_fullname" name="edit_fullname">
            </div>
            <div class="form-group">
              <label for="username" class="col-form-label">Usuario:</label>
              <input type="text" class="form-control" id="edit_username" name="edit_username">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Restablecer contraseña:</label>
              <div class="input-group">
                <input type="password" class="form-control" id="edit_pass" name="edit_pass">
                <div class="input-group-append">
                  <button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="role">Rol:</label>
              <select class="form-control" name="edit_role" id="edit_role">
                <option value="R" <?= isset($role) && ($role == "R") ? "selected" : "" ?>>Regular</option>
                <option value="S" <?= isset($role) && ($role == "S") ? "selected" : "" ?>>Superusuario</option>
              </select>
            </div>
            <div class="form-group">
              <label for="role">Estado:</label>
              <select class="form-control" name="edit_blocked" id="edit_blocked">
                <option value="Y" <?= isset($blocked) && ($blocked == "Y") ? "selected" : "" ?>>Bloqueado</option>
                <option value="N" <?= isset($blocked) && ($blocked == "N") ? "selected" : "" ?>>Desbloqueado</option>
              </select>
            </div>
            <br>
            <button type="submit" class="btn btn-danger" aria-hidden="true">Guardar cambios</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>


  <?php include VIEWS . '/partials/footer.php' ?>
  <script>
    $('#delete').on('show.bs.modal', function(event) {
      var info = $(event.relatedTarget)
      $('#delete_id').val(info.data('id'));
      $('#delete_name').val(info.data('name'));
      $('#delete_btn').attr("href", "index.php?action=delete&id=" + info.data('id'));
    })

    $('#edit').on('show.bs.modal', function(event) {
      var info = $(event.relatedTarget) // Botón que activó el modal
      // Extraer la información de atributos de datos y mostrar
      $('#edit_id').val(info.data('id'));
      $('#edit_fullname').val(info.data('name'));
      $('#edit_username').val(info.data('user'));
      $('#edit_pass').val(info.data('pass'));
      if (info.data('role') == 'N') {
        $("#edit_role").val("N");
      } else {
        $("#edit_role").val("R");
      }
      if (info.data('state') == 'N') {
        $("#edit_blocked").val("N");
      } else {
        $("#edit_blocked").val("Y");
      }
      //Tomar datos nuevos



      $('#edit_btn').attr("href", "index.php?action=delete&id=" + info.data('id') + '&name=');
    })

    $('#view').on('show.bs.modal', function(event) {
      var info = $(event.relatedTarget)
      $('#view_id').val(info.data('id'));
      $('#view_fullname').val(info.data('name'));
      $('#view_username').val(info.data('user'));
      //$('#view_role').val(info.data('role'));
      //$('#view_state').val(info.data('state'));
      if (info.data('role') == 'S') {
        $('#view_role').val('Superusuario');
      } else {
        $('#view_role').val('Regular');
      }
      if (info.data('state') == 'N') {
        $('#view_state').val('Desbloqueado');
      } else {
        $('#view_state').val('Bloqueado');
      }

      //$('#edit_btn').attr("href", "index.php?action=delete&id=" + info.data('id') + '&name=');
    })
    $("#actualizarDatos").submit(function(event) {
      var parametros = $(this).serialize();
      $.ajax({
        type: "POST",
        url: "users/index.php?action=update",
        data: parametros,
        beforeSend: function(objeto) {
          $("#datos_ajax").html("Mensaje: Cargando...");
        },
        success: function(datos) {
          $("#datos_ajax").html(datos);

          load(1);
        }
      });
      event.preventDefault();
    });



    function mostrarPassword() {
      var cambio = document.getElementById("edit_pass");
      if (cambio.type == "password") {
        cambio.type = "text";
        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
      } else {
        cambio.type = "password";
        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
      }
    }
  </script>
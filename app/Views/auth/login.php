<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';
?>
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php include VIEWS . '/partials/message.php' ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <br>
            <h1>Iniciar sesión</h1>
            <!-- Inicia el formulario de Login -->
            <form action="/authenticate/index.php?action=auth" method="post">
                <div class="form-group">
                    <label for="username">Usuario:</label>
                    <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="">
                </div>
                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="">
                </div>
                <button type="submit" class="btn btn-primary">Iniciar sesión</button>
                <a href="" class="btn btn-danger">Cancelar</a>
                <div>
                    No eres usuario aún? <a href="/authenticate/index.php?action=create" class="card-link">Registrarse</a>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>
<?php include VIEWS . '/partials/header.php' ?>
<?php include VIEWS . '/partials/navbar.php' ?>
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php include VIEWS . '/partials/message.php' ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h1>Nuevo usuario</h1>
            <!-- Inicia el formulario de Login -->
            <form action="/authenticate/index.php?action=register" method="post">
                <div class="form-group">
                    <label for="fullname">Nombre completo:</label>
                    <input type="text" class="form-control" id="fullname" name="fullname" aria-describedby="Introduzca su nombre del usuario" placeholder="" value=<?= isset($fullname) ? $fullname : "" ?>>
                </div>
                <div class="form-group">
                    <label for="username">Usuario:</label>
                    <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="" value=<?= isset($username) ? $username : "" ?>>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="" value=<?= isset($password) ? $password : "" ?>>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirme contraseña:</label>
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="" value=<?= isset($confirm_password) ? $confirm_password : "" ?>>
                </div>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a class="btn btn-danger" href="/users/index.php?action=register">Cancelar</a>
            </form>
        </div>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>
<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';

$cuestionario = $_GET['id'];
?>
<div class="container">
    <div class="container">
        <br>
        <h1>Mantenimiento de Preguntas</h1>
        <div class="form-group">
            <label for="username">Cuestionario:</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="">
        </div>

        <table class="table table-hover">
            <thead>
                <tr class="table-success">
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">PREGUNTAS</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($questionCollection as $record) : ?>
                    <tr>
                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#view" data-id="<?= $record["id"] ?>" data-pregunta="<?= $record["question_text"] ?>" data-cuestionario="<?= $record["description"] ?>" href="#">Ver</a></td>
                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#edit" data-id="<?= $record["id"] ?>" data-pregunta="<?= $record["question_text"] ?>" data-cuestionario="<?= $record["description"] ?>" data-quest="<?= $record["questionnaire_id"] ?>" href="#">Editar</a></td>
                        <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="<?= $record["id"] ?>" data-quest="<?= $record["questionnaire_id"] ?>" data-pregunta="<?= $record["question_text"] ?>" href="#">Eliminar</button></td>
                        <td><a class="btn btn-warning" href="<?= "/answer/index.php?action=index&id=" . $record['id']; ?>">Respuestas</a></td>
                        <td id="description"><?= $record["question_text"] ?></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
        <div>
            <?php if (count($questionCollection) == 0) : ?>
                <h4 class="text-danger">Cuestionario aún sin preguntas!!! Agrega algunas</h4>
            <?php endif; ?>
        </div>
        <a class="btn btn-success" data-toggle="modal" data-target="#new" data-cuest="<?= $cuestionario ?>" data-cuestionario="<?= $record["description"] ?>" href="#">Agregar Pregunta</a>
        <a class="btn btn-primary" href="/questionaires/index.php?action=index">Regresar a lista de cuestionarios</a>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>



<!-- POP UP ELIMINAR -->
<!-- Modal -->

<div class="modal" id="delete">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">ELIMINAR PREGUNTA</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <h5>Está seguro de eliminar la siguiente pregunta:</h5>
                        <input style="display:none" disabled type="text" class="form-control" id="delete_idPregunta">
                        <input style="display:none" disabled type="text" class="form-control" id="delete_idQuest">
                        <input disabled type="text" class="form-control" id="delete_question">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger" aria-hidden="true" href="" id="delete_btn" onclick="deleteQuestion();">Eliminar</a>
            </div>
        </div>
    </div>
</div>

<!-- POP UP ver -->
<!-- Modal -->
<div class="modal" id="view">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">VER PREGUNTA</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset disabled>
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="view_id">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input type="text" class="form-control" id="view_cuestionario">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Pregunta:</label>
                        <input type="text" class="form-control" id="view_pregunta">
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ver reporte</button>
                <a class="btn btn-danger" data-toggle="modal" data-target="#edit" aria-hidden="true">Editar</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar a la lista</button>
            </div>
        </div>
    </div>
</div>
<!-- POP UP edit -->
<!-- Modal -->
<div class="modal" id="edit">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EDITAR PREGUNTA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">

                    <label for="fullname" class="col-form-label">Cuestionario:</label>
                    <input disabled type="text" class="form-control" id="edit_cuestionario">

                    <input style="display:none" disabled type="text" class="form-control" id="edit_idPregunta">
                    <input style="display:none" disabled type="text" class="form-control" id="edit_idQuest">
                </div>
                <div class="form-group">
                    <label for="username" class="col-form-label">Pregunta:</label>
                    <input type="text" class="form-control" id="edit_pregunta">
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" aria-hidden="true" id="edit_btn" onclick="editQuestion();" href="">Guardar</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="new">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NUEVA PREGUNTA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row">
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input style="display:none" disabled type="text" class="form-control" id="new_cuestID">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input disabled type="text" class="form-control" id="new_description">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Pregunta:</label>
                        <input type="text" class="form-control" id="new_question">
                    </div>
                    <p class="text-danger" id="new_message"></p>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" aria-hidden="true" href="" id="new_btn" onclick="saveQuestion()">Guardar</a>
                <a type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</a>
            </div>
        </div>
    </div>
</div>


<?php include VIEWS . '/partials/footer.php' ?>
<script>
    $('#delete').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#delete_idPregunta').val(info.data('id'));
        $('#delete_idQuest').val(info.data('quest'));
        $('#delete_question').val(info.data('pregunta'));
        $('#delete_btn').attr("href", "index.php?action=delete&id=" + info.data('id') + '$quest=') + info.data('questId');
    })

    $('#edit').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#edit_cuestionario').val(info.data('cuestionario'));
        $('#edit_pregunta').val(info.data('pregunta'));

        $('#edit_idPregunta').val(info.data('id'));
        $('#edit_idQuest').val(info.data('quest'));
    })

    $('#new').on('show.bs.modal', function(event) {
        // obtener datos del form
        var info = $(event.relatedTarget)
        //muestra nombre del cuestionario
        $('#new_description').val(info.data('cuestionario'));
        //muestra id del cuestionario en input oculto
        $('#new_cuestID').val(info.data('cuest'));
    })

    $('#view').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#view_id').val(info.data('id'));
        $('#view_cuestionario').val(info.data('cuestionario'));
        $('#view_pregunta').val(info.data('pregunta'));
    })


    $('#edit').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
                url: $(this).attr('questionaires/index.php?action=update'),
                type: $(this).attr('POST'),
                data: {
                    data: $(this).serializeArray()
                }
            })
            .done(function(response) {
                if (response.status === "OK") {
                    $('#edit').modal('hide');
                } else {
                    // mostrar el error al usuario
                }
            });
    });


    function saveQuestion() {
        //mandar a guardar datos
        var idQuest = $('#new_cuestID').val();
        var pregunta = $('#new_question').val();
        $('#new_btn').attr("href", "index.php?action=store&id=" + idQuest + '&question=' + pregunta);
        $('#new_question').val("");
    }

    function deleteQuestion() {
        //mandar a guardar datos
        var idQuest = $('#delete_idQuest').val();
        var idPregunta = $('#delete_idPregunta').val();
        $('#delete_btn').attr("href", "index.php?action=delete&id=" + idQuest + '&question=' + idPregunta);
    }

    function editQuestion() {
        //mandar a guardar datos
        var idQuest = $('#edit_idQuest').val();
        var idPregunta = $('#edit_idPregunta').val();
        var pregunta = $('#edit_pregunta').val();
        $('#edit_btn').attr("href", "index.php?action=update&id=" + idQuest + '&question=' + idPregunta + '&text=' + pregunta);
        $('#edit_pregunta').val("");
        $('#edit_idPregunta').val("");
    }
</script>
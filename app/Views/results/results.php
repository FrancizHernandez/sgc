<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';


?>
<div class="container">
    <div class="container">
        <br>
        <h1>Mis Resultados</h1>
        <div class="form-group">
            <label for="username">Empleado:</label>
            <input disabled type="text" class="form-control" value="<?php echo $login["fullname"] ?>" aria-describedby="Introduzca su nombre de usuario" placeholder="">
        </div>

        <table class="table table-hover">
            <thead>
                <tr class="table-success">
                    <th scope="col">CUESTIONARIO</th>
                    <th scope="col">PUNTOS</th>
                    <th scope="col">RESULTADO</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultCollection as $record) : ?>
                    <tr>
                        <td id=""><?= $record["description"] ?></td>
                        <td id=""><?= $record["result"] ?></td>
                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#view" data-fullname="<?= $record["fullname"] ?>" data-description="<?= $record["description"] ?>" data-feedback="<?= $record["feedback"] ?>" data-result="<?= $record["result"] ?>" href="#">Ver</a></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
        <div>
            <br><br>
            <?php if (count($resultCollection) == 0) : ?>
                <h4 class="text-danger">Aún No has contestado ningun cuestionario!!! Inténtalo.</h4>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>


<!-- POP UP ver -->
<!-- Modal -->
<div class="modal" id="view">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">VER RESULTADO</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset disabled>
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="view_id">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input type="text" class="form-control" id="view_description">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Empleado:</label>
                        <input type="text" class="form-control" id="view_fullname">
                    </div>
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="view_id">
                        <label for="fullname" class="col-form-label">Puntos:</label>
                        <input type="text" class="form-control" id="view_ptos">
                    </div>
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="view_id">
                        <label for="fullname" class="col-form-label">Retroalimentacion:</label>
                        <textarea class="form-control" id="view_feedback" rows="3"></textarea>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar a mis resultados</button>
            </div>
        </div>
    </div>
</div>


<?php include VIEWS . '/partials/footer.php' ?>
<script>
    $('#view').on('show.bs.modal', function(event) {
        // obtener datos del form
        var info = $(event.relatedTarget)
        $('#view_fullname').val(info.data('fullname'));
        $('#view_description').val(info.data('description'));
        $('#view_feedback').val(info.data('feedback'));
        $('#view_ptos').val(info.data('result'));
    })
</script>
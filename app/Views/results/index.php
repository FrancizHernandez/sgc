<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';


?>
<div class="container">
    <div class="container">
        <br>
        <h1>Mantenimiento de Resultados</h1>
        <div class="form-group">
            <label for="username">Cuestionario:</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="Introduzca su nombre de usuario" placeholder="">
        </div>

        <table class="table table-hover">
            <thead>
                <tr class="table-success">
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">RESULTADO</th>
                    <th scope="col">MIN</th>
                    <th scope="col">MAX</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultCollection as $record) : ?>
                    <tr>
                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#view" data-id="<?= $record["id"] ?>" data-questid="<?= $record["questionnaire_id"] ?>" data-min="<?= $record["min_value"] ?>" data-max="<?= $record["max_value"] ?>" data-feedback="<?= $record["feedback"] ?>" data-cuestionario="<?= $record["description"] ?>" href="#">Ver</a></td>
                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#edit" data-id="<?= $record["id"] ?>" data-questid="<?= $record["questionnaire_id"] ?>" data-min="<?= $record["min_value"] ?>" data-max="<?= $record["max_value"] ?>" data-feedback="<?= $record["feedback"] ?>" data-cuestionario="<?= $record["description"] ?>" href="#">Editar</a></td>
                        <td><a class="btn btn-danger" data-toggle="modal" data-target="#delete" data-id="<?= $record["id"] ?>" data-questid="<?= $record["questionnaire_id"] ?>" data-min="<?= $record["min_value"] ?>" data-max="<?= $record["max_value"] ?>" data-feedback="<?= $record["feedback"] ?>" href="#">Eliminar</a></td>
                        <td id="description"><?= $record["feedback"] ?></td>
                        <td id="description"><?= $record["min_value"] ?></td>
                        <td id="description"><?= $record["max_value"] ?></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
        <div>
            <?php if (count($resultCollection) == 0) : ?>
                <h4 class="text-danger">Cuestionario aún sin resultados!!! Agrega algunos</h4>
            <?php endif; ?>
        </div>
        <a class="btn btn-success" data-toggle="modal" data-target="#new" data-questid="<?= $record["questionnaire_id"] ?>" data-cuestionario="<?= $record["description"] ?>" href="#">Agregar Resultado</a>
        <a class="btn btn-primary" href="/questionaires/index.php?action=index">Regresar a lista de cuestionarios</a>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>



<!-- POP UP ELIMINAR -->
<!-- Modal -->

<div class="modal" id="delete">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <h5>Está seguro de eliminar el siguiente resultado:</h5>
                        <input style="display:none" disabled type="text" class="form-control" id="delete_id">
                        <input style="display:none" disabled type="text" class="form-control" id="delete_idQuest">
                        <input disabled type="text" class="form-control" id="delete_result">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger" aria-hidden="true" href="" id="delete_btn" onclick="deleteResult();">Eliminar</a>
            </div>
        </div>
    </div>
</div>

<!-- POP UP ver -->
<!-- Modal -->
<div class="modal" id="view">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">VER RESULTADO</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset disabled>
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="view_id">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input type="text" class="form-control" id="view_cuestionario">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Respuesta:</label>
                        <input type="text" class="form-control" id="view_feedback">
                        <input style="display:none" type="text" class="form-control" id="view_feedbackid">
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Min</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="view_min">
                            </div>
                        </div>
                        <div class="col">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Max</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="view_max">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ver reporte</button>
                <a class="btn btn-danger" data-toggle="modal" data-target="#edit" aria-hidden="true">Editar</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar a la lista</button>
            </div>
        </div>
    </div>
</div><!-- POP UP ELIMINAR -->
<!-- Modal -->

<!-- POP UP edit -->
<!-- Modal -->
<div class="modal" id="edit">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EDITAR RESPUESTA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="index.php?action=update" method="POST">
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="edit_id" name="edit_id">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input disabled type="text" class="form-control" id="edit_cuestionario" name="edit_cuestionario">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Respuesta:</label>
                        <input type="text" class="form-control" id="edit_feedback" name="edit_feedback">
                        <input style="display:none" type="text" class="form-control" id="edit_feedbackid" name="edit_feedbackid">
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Min</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="edit_min" name="edit_min">
                            </div>
                        </div>
                        <div class="col">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Max</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="edit_max" name="edit_max">
                            </div>
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-danger" aria-hidden="true">Guardar cambios</button>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="new">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NUEVA RESPUESTA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row">
            </div>
            <div class="modal-body">
                <form action="index.php?action=store" method="POST" name="form_answers">
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="new_idquest" name="new_idquest">
                        <label for="fullname" class="col-form-label">Cuestionario:</label>
                        <input disabled type="text" class="form-control" id="new_cuestionario" name="edit_cuestionario">
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-form-label">Respuesta:</label>
                        <input type="text" class="form-control" id="new_feedback" name="new_feedback">
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Min</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="new_min" name="new_min">
                            </div>
                        </div>
                        <div class="col">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Max</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="new_max" name="new_max">
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" id="new_message"></p>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>

            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</a>
            </div>
        </div>
    </div>
</div>
<?php include VIEWS . '/partials/footer.php' ?>
<script>
    $('#delete').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#delete_id').val(info.data('id'));
        $('#delete_idQuest').val(info.data('questid'));
        $('#delete_result').val(info.data('feedback'));
        $('#delete_btn').attr("href", "index.php?action=delete&id=" + info.data('id') + '&quest=') + info.data('questId');
    })

    $('#edit').on('show.bs.modal', function(event) {
        // obtener datos del form
        var info = $(event.relatedTarget)
        //muestra nombre del cuestionario
        $('#edit_id').val(info.data('questid'));
        //muestra id del cuestionario en input oculto
        $('#edit_cuestionario').val(info.data('cuestionario'));
        $('#edit_feedback').val(info.data('feedback'));
        $('#edit_feedbackid').val(info.data('id'));

        $('#edit_min').val(info.data('min'));
        $('#edit_max').val(info.data('max'));
    })

    $('#new').on('show.bs.modal', function(event) {
        // obtener datos del form
        var info = $(event.relatedTarget)
        $('#new_idquest').val(info.data('questid'));
        //muestra id del cuestionario en input oculto
        $('#new_cuestionario').val(info.data('cuestionario'));
    })

    $('#view').on('show.bs.modal', function(event) {
        // obtener datos del form
        var info = $(event.relatedTarget)
        //muestra nombre del cuestionario
        $('#view_id').val(info.data('questid'));
        //muestra id del cuestionario en input oculto
        $('#view_cuestionario').val(info.data('cuestionario'));
        $('#view_feedback').val(info.data('feedback'));
        $('#view_feedbackid').val(info.data('id'));

        $('#view_min').val(info.data('min'));
        $('#view_max').val(info.data('max'));
    })



    function saveQuestion() {
        //mandar a guardar datos
        var idQuest = $('#new_cuestID').val();
        var pregunta = $('#new_question').val();
        $('#new_btn').attr("href", "index.php?action=store&id=" + idQuest + '&question=' + pregunta);
        $('#new_question').val("");
    }

    function deleteResult() {
        //mandar a guardar datos
        var id = $('#delete_id').val();
        var idQuest = $('#delete_idQuest').val();
        $('#delete_btn').attr("href", "index.php?action=delete&id=" + id + '&quest=' + idQuest);
    }

    function updateResult() {
        //mandar a guardar datos
        var idQuest = $('#edit_idQuest').val();
        var idresult = $('#edit_idQuest').val();
        var result = $('#edit_idPregunta').val();
        var min = $('#edit_pregunta').val();
        var max = $('#edit_pregunta').val();
        $('#edit_btn').attr("href", "index.php?action=update&id=" + idQuest + '&question=' + idPregunta + '&text=' + pregunta);
        $('#edit_pregunta').val("");
        $('#edit_idPregunta').val("");
    }
</script>
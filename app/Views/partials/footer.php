    <footer>
        <!-- Footer -->
        <nav class="navbar fixed-bottom align-content-sm-end navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                        <p class="h6">© All right Reversed.<a class="text-green ml-2" href="" target="_blank">ADASotf</a></p>
                    </div>
                    <hr>
                </div>
            </div>
        </nav>
        <!-- ./Footer -->
        <script src="/assets/js/jquery-3.3.1.min.js" charset="utf-8"></script>
        <script src="/assets/js/bootstrap.bundle.min.js" charset="utf-8"></script>
        <script src="/assets/js/main.js" charset="utf-8"></script>
    </footer>
    </body>
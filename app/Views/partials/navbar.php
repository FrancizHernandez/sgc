<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <img src="/images/logo.png" alt="" width="40px">
  <a class="navbar-brand" href="/index.php">Avansoft</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/index.php">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <!--li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li-->
      <?php if (isset($login) && !is_null($login)) : ?>
        <?php if ($login['role'] == 'N') : ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Catálogos
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/users/index.php?action=index">Mantenimiento Usuarios</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/questionaires/index.php?action=index">Manteminiento Cuestionarios</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/questions/index.php?action=view">Manteminiento Preguntas</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/answer/index.php?action=index">Manteminiento Respuestas</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/results/index.php?action=index">Manteminiento Resultados</a>
            </div>
          </li>
        <?php endif; ?>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Cuestionarios
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/questionaires/index.php?action=questionaires">Ver todos los cuestionarios</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/results/index.php?action=myResults">Mis Resultados</a>
        </li>
    </ul>
  <?php else : ?>
    </ul>
  <?php endif; ?>
  ​
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <?php if (isset($login) && !is_null($login)) : ?>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= $login['username'] ?>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="/authenticate/index.php?action=logout.php">Cerrar sesión</a>
      </div>
    </li>
  <?php else : ?>
    <li class="nav-item">
      <a class="nav-link" href="/authenticate/index.php?action=login">Iniciar sesión</a>
    </li>
  <?php endif; ?>
  </li>
  </ul>
  ​
  </div>

</nav>
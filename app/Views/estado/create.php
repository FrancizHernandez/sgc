<?php include VIEWS . '/partials/header.php' ?>
<?php include VIEWS . '/partials/navbar.php' ?>
<div class="container">
    <br>
    <div class="row">
        <div class="col-md-12">
            <?php include VIEWS . '/partials/message.php' ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <!-- Inicia el formulario de Login -->
            <div class="container">
                <div class="row">
                    <div class="col-10">
                        <h1>Nuevo estado</h1>
                    </div>
                    <div class="col">
                        <a href="/estado/index.php?action=store" style="text-decoration-color: orangered;">
                            <i class="fas fa-sign-out-alt fa-2x"></i>
                        </a>
                    </div>
                </div>
                <br>
                <form action="/users/index.php?action=store" method="post">
                    <div class="form-group row">
                        <label for="fullname" class="col-sm-4 col-form-label">Descripción:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="fullname" name="fullname" aria-describedby="Introduzca su nombre del usuario" placeholder="" value=<?= isset($fullname) ? $fullname : "" ?>>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a class="btn btn-danger" href="/estado/index.php?action=store">Cancelar</a>
                </form>
            </div>
        </div>
    </div>
    <?php include VIEWS . '/partials/footer.php' ?>
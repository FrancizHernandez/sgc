<?php
include VIEWS . '/partials/header.php';
include VIEWS . '/partials/navbar.php';
include VIEWS . '/partials/pillsAsociado.php';
?>
<div class="col-10">
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <br>

                    <br>
                    <div class="row">
                        <div class="col-10">
                            <h3 class="text-muted">Tipos de estado de afiliados</h3>

                        </div>
                        <div class="col">
                            <a href="/index.php" style="text-decoration-color: orangered;">
                                <i class="fas fa-sign-out-alt fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    <br>

                    <table id="users" class="table table-hover">
                        <thead>
                            <tr class="table-success">
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($estadoCollection as $record) : ?>
                                <tr>
                                    <td><a class="fas fa-plus-square fa-2x" data-toggle="modal" data-target="#edit1" href="#"></td>
                                    <td><a class="far fa-eye fa-2x" data-toggle="modal" data-target="#view" data-id="<?= $record["id"] ?>" data-description="<?= $record["descripcion"] ?>" href="#"></td>
                                    <td><a class="far fa-edit fa-2x" data-toggle="modal" data-target="#edit" data-id="<?= $record["id"] ?>" data-description="<?= $record["descripcion"] ?>" href="#"></a></td>
                                    <th scope="row" style="display:none" id="id"><?= $record["id"] ?></th>
                                    <td id="fullname" data-id="<?= $record["descripcion"] ?>"><?= $record["descripcion"] ?></td>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- POP UP ver -->
<!-- Modal -->
<div class="modal" id="view">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>
                    <h4 class="modal-title">VER ESTADO</h4>
                </b>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <fieldset disabled>
                    <div class="form-group">
                        <input style="display:none" type="text" class="form-control" id="view_id">
                        <label for="descripcion" class="col-form-label">Descripción:</label>
                        <input type="text" class="form-control" id="view_description">
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Regresar a la lista</button>
            </div>
        </div>
    </div>
</div>
<!-- POP UP edit -->
<!-- Modal -->
<div class="modal" id="edit">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EDITAR CUESTIONARIO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input style="display:none" type="text" class="form-control" id="edit_id">
                    <label for="fullname" class="col-form-label">Descripción:</label>
                    <input type="text" class="form-control" id="edit_description">
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" aria-hidden="true" id="edit_btn" onclick="updateEstado();" href="">Guardar</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>



<div class="modal" id="edit1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">AGREGAR ESTADO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input style="display:none" type="text" class="form-control" id="edit_id">
                    <label for="fullname" class="col-form-label">Descripción:</label>
                    <input type="text" class="form-control" id="new_description">
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" aria-hidden="true" id="new_btn" onclick="saveEstado();" href="">Guardar</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>



<?php include VIEWS . '/partials/footer.php' ?>
<script>
    // muestra la informacion en el modal
    $('#edit').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget) // Botón que activó el modal
        // Extraer la información de atributos de datos y mostrar
        $('#edit_id').val(info.data('id'));
        $('#edit_description').val(info.data('description'));
        $('#edit_longDescription').val(info.data('long'));
        //Tomar datos nuevos
        //$('#edit_btn').attr("href", "index.php?action=delete&id=" + info.data('id') + '&name=');
    })

    //obtine los datos ingresados en el modal
    $('#new').on('show.bs.modal', function(event) {
        // obtener datos del form
        desc = $('#new_description').val();
        //mandar a guardar datos
        $('#new_btn').attr("href", "index.php?action=store&description=" + desc + '&long_description=' + long);

    })

    // muestra los datos en el modal ver
    $('#view').on('show.bs.modal', function(event) {
        var info = $(event.relatedTarget)
        $('#view_id').val(info.data('id'));
        $('#view_description').val(info.data('description'));

    })
    //actualiza el estado
    function updateEstado() {
        //mandar a guardar datos
        var id = $('#edit_id').val();
        var desc = $('#edit_description').val();
        $('#edit_btn').attr("href", "/estado/index.php?action=update&id=" + id + "&desc=" + desc);
        $('#edit_description').val("");
    }

    function saveEstado() {
        //mandar a guardar datos
        var desc = $('#new_description').val();
        $('#new_btn').attr("href", "/estado/index.php?action=store&desc=" + desc);
        $('#new_description').val("");
    }
</script>
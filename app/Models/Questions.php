<?php

namespace MyApp\Models {

    use EasilyPHP\Database\DBMySQL;

    class Questions
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'],
                $config['database'],
                $config['user'],
                $config['password']
            );
        }

        public function getAll()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM questions;");
            $this->db->disconnect();
            return $this->db->getall($result);
        }


        public function get_Questionnaire_Questions($id)
        {
            $this->db->connect();
            // $result = $this->db->runSql("SELECT * FROM questions WHERE questionnaire_id=$id;");
            $result = $this->db->runSql("SELECT c.id, c.questionnaire_id, c.question_text, q.description
            FROM questions c
            INNER JOIN questionnaires q
            ON q.id=c.questionnaire_id
            WHERE questionnaire_id=" . $id);
            $this->db->disconnect();
            return $this->db->getall($result);
        }

        public function insert($id, $question)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("INSERT INTO questions(`questionnaire_id`, `question_text`) 
                    VALUES (?, ?)"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("ss", $id, $question)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function update($id, $text)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("UPDATE questions SET `question_text`= ? WHERE `id` =?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("ss", $text, $id)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function delete($id)
        {
            $this->db->connect();
            $sql = "DELETE FROM questions WHERE id = ?";

            if ($stmt = $this->db->prepareSQL($sql)) {
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->close();
            } else {
                echo $this->db->getError();
                exit;
            }
            $this->db->disconnect();
        }
    }
}

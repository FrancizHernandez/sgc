<?php

namespace MyApp\Models {

    use EasilyPHP\Database\DBMySQL;

    class Resolver
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'],
                $config['database'],
                $config['user'],
                $config['password']
            );
        }

        public function getPreguntas($idQuest)
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM questions where questionnaire_id=" . $idQuest);
            $this->db->disconnect();
            return $this->db->getAll($result);
        }

        public function getRespuestas($idPregunta)
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM answers where question_id=" .
                $idPregunta . " order by number");
            $this->db->disconnect();
            return $this->db->getAll($result);
        }
    }
}

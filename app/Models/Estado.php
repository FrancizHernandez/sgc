<?php

namespace MyApp\Models {

    use EasilyPHP\Database\DBMySQL;

    class Estado
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'],
                $config['database'],
                $config['user'],
                $config['password']
            );
        }

        public function getAllEstado()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM estado_afiliado");
            $this->db->disconnect();
            return $this->db->getAll($result);
        }

        public function insert($description)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("INSERT INTO estado_afiliado(`descripcion`) 
                    VALUES (?)"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("s", $description)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function update($id, $description)
        {
            $this->db->connect();
            $sql = "UPDATE estado_afiliado SET `descripcion` = ? WHERE `id`=?";

            $stmt = $this->db->prepareSQL($sql);
            if ($stmt) {
                $stmt->bind_param("ss", $description, $id);
                $stmt->execute();
                $stmt->close();
            } else {
                echo $this->db->getError();
                exit;
            }
            $this->db->disconnect();
        }

        public function estadoExists($description)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("SELECT count(1) as `exists` FROM estado_afiliado WHERE `descripcion` = ?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("s", $description)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $result = $stmt->get_result();
            return $this->db->nextResultRow($result);
        }
    }
}

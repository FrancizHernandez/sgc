<?php

namespace MyApp\Models {

    use EasilyPHP\Database\DBMySQL;

    class Afiliado
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'],
                $config['database'],
                $config['user'],
                $config['password']
            );
        }

        public function getAllAfiliado()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM afiliado");
            $this->db->disconnect();
            return $this->db->getAll($result);
        }

        public function insert($description, $longDescription)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("INSERT INTO questionnaires(`description`, `long_description`) 
                    VALUES (?, ?)"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("ss", $description, $longDescription)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function update($id, $description, $longDescription)
        {
            $this->db->connect();
            $sql = "UPDATE questionnaires SET `description` = ?, `long_description` = ? WHERE `id`=?";

            $stmt = $this->db->prepareSQL($sql);
            if ($stmt) {
                $stmt->bind_param("sss", $description, $longDescription, $id);
                $stmt->execute();
                $stmt->close();
            } else {
                echo $this->db->getError();
                exit;
            }
            $this->db->disconnect();
        }

        public function delete($id)
        {
            $this->db->connect();
            $sql = "DELETE FROM questionnaires WHERE id = ?";

            if ($stmt = $this->db->prepareSQL($sql)) {
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->close();
            } else {
                echo $this->db->getError();
                exit;
            }
            $this->db->disconnect();
        }
    }
}

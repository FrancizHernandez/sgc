<?php

namespace MyApp\Models {

    use EasilyPHP\Database\DBMySQL;

    class Result
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'],
                $config['database'],
                $config['user'],
                $config['password']
            );
        }

        public function getAll()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM results;");
            $this->db->disconnect();
            return $this->db->getall($result);
        }


        public function get_Questionnaire_Results($id)
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT r.id, r.questionnaire_id,  r.min_value, r.max_value,  r.feedback, q.description
            FROM results r 
            INNER JOIN questionnaires q
            ON r.questionnaire_id=q.id
            WHERE q.id=" . $id);
            $this->db->disconnect();
            return $this->db->getall($result);
        }

        public function getResultsByUser($idUser)
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT users.id, users.fullname, questionnaires.description, results.feedback,users_questionnaires.questionnaire_id,
                      users_questionnaires.result, results.min_value, results.max_value
                      FROM ((users_questionnaires
                      INNER JOIN  users ON users.id=users_questionnaires.user_id
                      INNER JOIN  questionnaires ON questionnaires.id = users_questionnaires.questionnaire_id
                      INNER JOIN  results ON questionnaires.id = results.questionnaire_id))
                      where users.id=" . $idUser . "
                      and results.min_value <= users_questionnaires.result 
                      and results.max_value >= users_questionnaires.result");
            $this->db->disconnect();
            return $this->db->getall($result);
        }

        public function insert($idquest, $resultado, $min, $max)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("INSERT INTO results(`questionnaire_id`, `min_value`, `max_value`, `feedback`) 
                    VALUES (?, ?, ?, ?)"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("ssss", $idquest, $min, $max, $resultado)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function update($id, $resultado, $min, $max)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("UPDATE results SET `min_value`= ?, `max_value`=?, `feedback`=? WHERE `id` =?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("ssss", $min, $max, $resultado, $id)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function delete($id)
        {
            $this->db->connect();
            $sql = "DELETE FROM results WHERE id = ?";

            if ($stmt = $this->db->prepareSQL($sql)) {
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->close();
            } else {
                echo $this->db->getError();
                exit;
            }
            $this->db->disconnect();
        }
    }
}

<?php

namespace MyApp\Models {

    use EasilyPHP\Database\DBMySQL;

    class LineasCredito
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'],
                $config['database'],
                $config['user'],
                $config['password']
            );
        }

        public function getAllLineasCredito()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM linea_credito");
            $this->db->disconnect();
            return $this->db->getAll($result);
        }

        public function insert($codigo, $description, $tasaInt)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("INSERT INTO linea_credito(`codigo`,`descripcion`,`tasa_interes`) 
                    VALUES (???)"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("sss", $codigo, $description, $tasaInt)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function update($id, $codigo, $description, $tasaInt)
        {
            $this->db->connect();
            $sql = "UPDATE linea_credito SET `codigo`=?, `descripcion` = ?, `tasa_interes`=? WHERE `id`=?";

            $stmt = $this->db->prepareSQL($sql);
            if ($stmt) {
                $stmt->bind_param("ssss", $codigo, $description, $tasaInt, $id);
                $stmt->execute();
                $stmt->close();
            } else {
                echo $this->db->getError();
                exit;
            }
            $this->db->disconnect();
        }

        public function ifExists($codigo)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("SELECT count(1) as `exists` FROM linea_credito WHERE `codigo` = ?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("s", $codigo)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $result = $stmt->get_result();
            return $this->db->nextResultRow($result);
        }
    }
}

<?php

namespace MyApp\Models {

    use EasilyPHP\Database\DBMySQL;

    class Answer
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'],
                $config['database'],
                $config['user'],
                $config['password']
            );
        }

        public function getAllQuest()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM questionnaires");
            $this->db->disconnect();
            return $this->db->getAll($result);
        }

        public function getAll()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM answers;");
            $this->db->disconnect();
            return $this->db->getall($result);
        }


        public function getMaxNumber($id)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("SELECT  max(number) FROM answers WHERE question_id= ?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("s", $id)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $result = $stmt->get_result();
            return $this->db->nextResultRow($result);
        }

        public function get_Question_Answers($id)
        {
            $this->db->connect();
            // $result = $this->db->runSql("SELECT * FROM questions WHERE questionnaire_id=$id;");
            $result = $this->db->runSql("SELECT a.id, a.question_id, a.number, a.answer_text, a.answer_points,
            q.description, q.id as qid, p.question_text
            FROM isw613_questionnaires.answers a
            INNER JOIN isw613_questionnaires.questions p
            ON p.id=a.question_id
            INNER JOIN isw613_questionnaires.questionnaires q
            ON p.questionnaire_id=q.id
            WHERE question_id=" . $id);
            $this->db->disconnect();
            return $this->db->getall($result);
        }


        public function insert($id, $answer, $number, $puntos)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("INSERT INTO answers (`question_id`, `answer_text`, `number`,`answer_points`) 
                    VALUES (?, ?, ?, ?)"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("ssss", $id, $answer, $number, $puntos)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function update($idRespuesta, $respuesta, $puntos)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt =
                $this->db->prepareSql("UPDATE answers SET `answer_text`= ? , `answer_points`=? WHERE `id` =?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("sss", $respuesta, $puntos, $idRespuesta)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function delete($id)
        {
            $this->db->connect();
            $sql = "DELETE FROM answers WHERE id = ?";

            if ($stmt = $this->db->prepareSQL($sql)) {
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->close();
            } else {
                echo $this->db->getError();
                exit;
            }
            $this->db->disconnect();
        }
    }
}

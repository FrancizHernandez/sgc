<?php

namespace MyApp\Controllers {

    use MyApp\Models\Resolver;
    use MyApp\Utils\Message;

    class ResolverController
    {
        private $config = null;
        private $login = null;
        private $answerModel = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index($questionaire)
        {
            $login = $this->login;
            $message = new Message();

            $resultModel = new Resolver($this->config);
            $resultCollection = $resultModel->get_Questionnaire_Results($questionaire);
            view("results/index.php", compact("resultCollection", "login"));
            exit;
        }
    }
}

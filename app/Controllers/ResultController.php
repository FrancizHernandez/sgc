<?php

namespace MyApp\Controllers {

    use MyApp\Models\Result;
    use MyApp\Utils\Message;

    class ResultController
    {
        private $config = null;
        private $login = null;
        private $answerModel = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index($questionaire)
        {
            $login = $this->login;
            $message = new Message();

            $resultModel = new Result($this->config);
            $resultCollection = $resultModel->get_Questionnaire_Results($questionaire);
            view("results/index.php", compact("resultCollection", "login"));
            exit;
        }

        // no se agrega el ->, compact("login") al view xq es la vista de registro nuevos usuarioss
        public function create()
        {
            view("users/create.php");
        }
        public function store()
        {
            $idquest = intval($_POST["new_idquest"]);
            $resultado = $_POST["new_feedback"];
            $min = $_POST["new_min"];
            $max = $_POST["new_max"];
            $login = $this->login;
            $message = new Message();
            $resultModel = new Result($this->config);

            // verificar q todos los inputs esten llenos
            if ($resultado == "" || $min == "" || $max == "") {
                $message->setWarningMessage(null, "El campo respuesta es requerido", null, true);
                $resultCollection = $resultModel->get_Questionnaire_Results($idquest);
                view("results/index.php", compact("resultCollection", "login"));
                exit;
            }
            $resultModel->insert($idquest, $resultado, $min, $max);
            $resultCollection = $resultModel->get_Questionnaire_Results($idquest);
            view("results/index.php", compact("resultCollection", "login"));
            exit;
        }
        public function update()
        {
            $idquest = intval($_POST["edit_id"]);
            $idResult = intval($_POST["edit_feedbackid"]);
            $resultado = $_POST["edit_feedback"];
            $min = $_POST["edit_min"];
            $max = $_POST["edit_max"];

            $login = $this->login;
            $message = new Message();
            $resultModel = new Result($this->config);

            //actualizar
            if ($resultado == "" || $min == "" || $max == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                $resultCollection = $resultModel->get_Questionnaire_Results($idquest);
                view("results/index.php", compact("resultCollection", "login"));
                exit;
            }

            $resultModel->update($idResult, $resultado, $min, $max);
            $resultCollection = $resultModel->get_Questionnaire_Results($idquest);
            view("results/index.php", compact("resultCollection", "login"));
            exit;
        }
        public function delete($id, $idquest)
        {
            $login = $this->login;
            $message = new Message();
            var_dump($id, $idquest);

            $resultModel = new Result($this->config);
            $resultModel->delete($id);
            $resultCollection = $resultModel->get_Questionnaire_Results($idquest);
            view("results/index.php", compact("resultCollection", "login"));
            exit;
        }

        public function getResultsByUserAct()
        {
            $login = $this->login;
            $message = new Message();
            $id = $login["id"];
            $resultModel = new Result($this->config);
            $resultCollection = $resultModel->getResultsByUser($id);
            view("results/results.php", compact("resultCollection", "login"));
            exit;
        }
    }
}

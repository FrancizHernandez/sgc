<?php

namespace MyApp\Controllers {

    use MyApp\Models\Afiliado;
    use MyApp\Utils\Message;

    class AfiliadoController
    {
        private $config = null;
        private $login = null;
        private $userModel = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index()
        {
            $login = $this->login;
            $message = new Message();

            $afiliadoModel = new Afiliado($this->config);
            $afiliadoCollection = $afiliadoModel->getAllAfiliado();
            view("afiliado/index.php", compact("afiliadoCollection", "login"));
            exit;
        }


        // no se agrega el ->, compact("login") al view xq es la vista de registro nuevos usuarioss
        public function create()
        {
            $login = $this->login;
            view("afiliado/create.php", compact("login"));
        }
        public function store($description, $longDescription)
        {
            $login = $this->login;
            $message = new Message();
            $questModel = new Afiliado($this->config);


            //$userModel = new ($this->config);
            // $result = $userModel->userExists($username);
            // verificar q todos los inputs esten llenos
            if ($description == "" || $longDescription == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                $questCollection = $questModel->getAllQuest();
                view("quest/index.php", compact("questCollection", "login"));
                exit;
            }
            /*if ($result["exists"] == 1) {
                $message->setWarningMessage(null, "Usuario ya existe", null, true);
                view("users/create.php", compact("message", "login", "fullname", "username", "password", "confirm_password", "role"));
                exit;
            }*/
            $questModel->insert($description, $longDescription);
            $questCollection = $questModel->getAllQuest();
            view("quest/index.php", compact("questCollection", "login"));
            exit;
        }
        public function update($id, $desc, $longDesc)
        {
            $login = $this->login;

            $questModel = new Afiliado($this->config);
            $message = new Message();

            //actualizar
            if ($desc == "" || $longDesc == "") {
                $message->setWarningMessage(null, "El campo pregunta es requerido", null, true);
                $questCollection = $questModel->getAllQuest();
                view("quest/index.php", compact("questCollection", "login"));
                exit;
            }
            $questModel->update($id, $desc, $longDesc);
            $questCollection = $questModel->getAllQuest();
            view("quest/index.php", compact("questCollection", "login"));
            exit;
        }
        public function delete($id)
        {
            $login = $this->login;
            $message = new Message();

            $questModel = new Afiliado($this->config);
            $questModel->delete($id);

            $questCollection = $questModel->getAllQuest();
            view("quest/index.php", compact("questCollection", "login"));
            //view("users/index.php?", compact("collection", "login"));
            exit;
        }
    }
}

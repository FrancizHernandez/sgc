<?php

namespace MyApp\Controllers {

    use MyApp\Models\Answer;
    use MyApp\Utils\Message;

    class AnswerController
    {
        private $config = null;
        private $login = null;
        private $answerModel = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index($question)
        {
            $login = $this->login;
            $message = new Message();

            $answerModel = new Answer($this->config);
            $answerCollection = $answerModel->get_Question_Answers($question);
            view("answer/index.php", compact("answerCollection", "login"));
            exit;
        }

        // no se agrega el ->, compact("login") al view xq es la vista de registro nuevos usuarioss
        public function create()
        {
            view("users/create.php");
        }
        public function store()
        {
            $idPregunta = $_POST["pregId"];
            $respuesta = $_POST["respuesta"];

            $puntos = $_POST["ptos"];

            $login = $this->login;
            $message = new Message();
            $answerModel = new Answer($this->config);
            $result = $answerModel->getMaxNumber($idPregunta);

            $number = intval($result["max(number)"]) + 1;
            var_dump($number);
            // verificar q todos los inputs esten llenos
            if ($respuesta == "" || $puntos == "") {
                $message->setWarningMessage(null, "El campo respuesta es requerido", null, true);
                $answerCollection = $answerModel->get_Question_Answers($idPregunta);
                view("answer/index.php", compact("answerCollection", "login"));
                exit;
            }
            $answerModel->insert($idPregunta, $respuesta, $number, $puntos);
            $answerCollection = $answerModel->get_Question_Answers($idPregunta);
            view("answer/index.php", compact("answerCollection", "login"));
            exit;
        }
        public function update()
        {

            $idPregunta = $_POST["edit_idpreg"];
            $idRespuesta = $_POST["edit_id"];
            $respuesta = $_POST["edit_respuesta"];
            $puntos = $_POST["edit_ptos"];

            var_dump($idPregunta, $idRespuesta, $respuesta, $puntos);
            $login = $this->login;
            $message = new Message();
            $answerModel = new Answer($this->config);

            //actualizar
            if ($respuesta == "" || $puntos == "") {
                $message->setWarningMessage(null, "El campo respuesta es requerido", null, true);
                $answerCollection = $answerModel->get_Question_Answers($idPregunta);
                view("answer/index.php", compact("answerCollection", "login"));
                exit;
            }

            $answerModel->update($idRespuesta, $respuesta, $puntos);
            $answerCollection = $answerModel->get_Question_Answers($idPregunta);
            view("answer/index.php", compact("answerCollection", "login"));
            exit;
        }
        public function delete($id, $question)
        {
            $login = $this->login;
            $message = new Message();

            $answerModel = new Answer($this->config);;
            $answerModel->delete($id);
            $answerCollection = $answerModel->get_Question_Answers($question);
            view("answer/index.php", compact("answerCollection", "login"));
            exit;
        }
    }
}

<?php

namespace MyApp\Controllers {

    use MyApp\Models\LineasCredito;
    use MyApp\Utils\Message;

    class LineasCreditoController
    {
        private $config = null;
        private $login = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index()
        {
            $login = $this->login;
            $message = new Message();

            $lineaModel = new LineasCredito($this->config);
            $lineaCollection = $lineaModel->getAllLineasCredito();
            view("lineaCredito/index.php", compact("lineaCollection", "login"));
            exit;
        }

        public function create()
        {
            $login = $this->login;
            view("lineaC/create.php", compact("login"));
        }
        public function store($codigo, $description, $tasaInt)
        {
            var_dump($description);
            $login = $this->login;
            $message = new Message();
            $lineaModel = new LineasCredito($this->config);
            $result = $lineaModel->ifExists($codigo);

            if ($codigo == "" || $description == "" || $tasaInt == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                $lineaCollection = $lineaModel->getAllLineasCredito();
                view("lineaC/create.php", compact("lineaCollection", "login"));
                exit;
            }
            if ($result["exists"] == 1) {
                $message->setWarningMessage(null, "Estado ya existe", null, true);
                view("lineaC/index.php", compact("message", "login"));
                exit;
            }
            $lineaModel->insert($codigo, $description, $tasaInt);
            $lineaCollection = $lineaModel->getAllLineasCredito();
            view("lineaC/index.php", compact("lineaCollection", "login"));
            exit;
        }
        public function update($id, $codigo, $description, $tasaInt)
        {
            $login = $this->login;

            $lineaModel = new LineasCredito($this->config);
            $message = new Message();

            //actualizar
            if ($codigo == "" || $description == "" || $tasaInt == "") {
                $message->setWarningMessage(null, "Todos los campos descripción es requerido", null, true);
                $lineaCollection = $lineaModel->getAllLineasCredito();
                view("lineaC/index.php", compact("lineaCollection", "login"));
                exit;
            }
            $lineaModel->update($id, $codigo, $description, $tasaInt);
            $lineaCollection = $lineaModel->getAllLineasCredito();
            view("lineaC/index.php", compact("lineaCollection", "login"));
            exit;
        }
    }
}

<?php

namespace MyApp\Controllers {

    use MyApp\Models\User;
    use MyApp\Utils\Message;

    class AuthController
    {
        private $config = null;
        private $login = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function login()
        {
            $dataToView = ["login" => $this->login];
            return view("auth/login.php", $dataToView);
        }

        public function create()
        {
            view("auth/register.php");
        }
        public function auth()
        {
            $userModel = new User($this->config);
            $login = $userModel->authenticate($_POST["username"], $_POST["password"]);

            if (!is_null($login)) {
                $_SESSION['login'] = $login;
                header("Location: /index.php");
            } else {
                $message = new Message();
                $message->setWarningMessage(null, "Usuario o contraseña invalida", null, true);
                //como no hubo atenticacion - se devuelve a la vista login
                // se hace compact creando arreglo asociativo y enviando a la vista 
                view("auth/login.php", compact("message", "login"));
            }
        }

        public function register()
        {
            $fullname         = $_POST["fullname"];
            $username         = $_POST["username"];
            $password         = $_POST["password"];
            $confirm_password = $_POST["confirm_password"];

            $userModel = new User($this->config);
            $message = new Message();
            $result = $userModel->userExists($username);
            // verificar q todos los inputs esten llenos
            if ($fullname == "" || $username == "" || $password == "" || $confirm_password == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                view("auth/register.php", compact("message", "fullname", "username", "password", "confirm_password"));
                exit;
            }
            if ($result["exists"] == 1) {
                $message->setWarningMessage(null, "Usuario ya existe", null, true);
                view("auth/register.php", compact("message", "fullname", "username", "password", "confirm_password"));
                exit;
            }
            if ($password != $confirm_password) {
                $message->setWarningMessage(null, "Las contraseñas no coinciden", null, true);
                view("auth/register.php", compact("message", "fullname", "username", "password", "confirm_password"));
                exit;
            }
            $userModel->register($fullname, $username, $password);
            view("auth/register.php", compact("message"));
            header("Location: /authenticate/index.php?action=login");
        }

        public function logout()
        {
            session_destroy();
            header("Location: /index.php");
        }
    }
}

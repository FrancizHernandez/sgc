<?php

namespace MyApp\Controllers {

    use MyApp\Models\Estado;
    use MyApp\Utils\Message;

    class EstadoController
    {
        private $config = null;
        private $login = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index()
        {
            $login = $this->login;
            $message = new Message();

            $estadoModel = new Estado($this->config);
            $estadoCollection = $estadoModel->getAllEstado();
            view("estado/index.php", compact("estadoCollection", "login"));
            exit;
        }


        public function create()
        {
            $login = $this->login;
            view("estado/create.php", compact("login"));
        }
        public function store($description)
        {
            var_dump($description);
            $login = $this->login;
            $message = new Message();
            $estadoModel = new Estado($this->config);
            $result = $estadoModel->estadoExists($description);

            if ($description == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                $estadoCollection = $estadoModel->getAllEstado();
                view("estado/create.php", compact("estadoCollection", "login"));
                exit;
            }
            if ($result["exists"] == 1) {
                $message->setWarningMessage(null, "Estado ya existe", null, true);
                view("estado/create.php", compact("message", "login", "descripcion"));
                exit;
            }
            $estadoModel->insert($description);
            $estadoCollection = $estadoModel->getAllEstado();
            view("estado/index.php", compact("estadoCollection", "login"));
            exit;
        }
        public function update($id, $desc)
        {
            $login = $this->login;

            $estadoModel = new Estado($this->config);
            $message = new Message();

            //actualizar
            if ($desc == "") {
                $message->setWarningMessage(null, "El campo descripción es requerido", null, true);
                $estadoCollection = $estadoModel->getAllEstado();
                view("estado/index.php", compact("estadoCollection", "login"));
                exit;
            }
            $estadoModel->update($id, $desc);
            $estadoCollection = $estadoModel->getAllEstado();
            view("estado/index.php", compact("estadoCollection", "login"));
            exit;
        }
    }
}

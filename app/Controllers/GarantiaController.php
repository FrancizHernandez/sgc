<?php

namespace MyApp\Controllers {

    use MyApp\Models\Garantia;
    use MyApp\Utils\Message;

    class GarantiaController
    {
        private $config = null;
        private $login = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index()
        {
            $login = $this->login;
            $message = new Message();

            $garantiaModel = new Garantia($this->config);
            $garantiaCollection = $garantiaModel->getAllgarantia();
            view("garantia/index.php", compact("garantiaCollection", "login"));
            exit;
        }


        public function create()
        {
            $login = $this->login;
            view("garantia/create.php", compact("login"));
        }
        public function store($description)
        {
            var_dump($description);
            $login = $this->login;
            $message = new Message();
            $garantiaModel = new Garantia($this->config);
            $result = $garantiaModel->estadoExists($description);

            if ($description == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                $garantiaCollection = $garantiaModel->getAllgarantia();
                view("garantia/create.php", compact("garantiaCollection", "login"));
                exit;
            }
            if ($result["exists"] == 1) {
                $message->setWarningMessage(null, "Estado ya existe", null, true);
                view("garantia/index.php", compact("message", "login"));
                exit;
            }
            $garantiaModel->insert($description);
            $garantiaCollection = $garantiaModel->getAllgarantia();
            view("garantia/index.php", compact("garantiaCollection", "login"));
            exit;
        }
        public function update($id, $desc)
        {
            $login = $this->login;

            $garantiaModel = new Garantia($this->config);
            $message = new Message();

            //actualizar
            if ($desc == "") {
                $message->setWarningMessage(null, "El campo descripción es requerido", null, true);
                $garantiaCollection = $garantiaModel->getAllgarantia();
                view("garantia/index.php", compact("garantiaCollection", "login"));
                exit;
            }
            $garantiaModel->update($id, $desc);
            $garantiaCollection = $garantiaModel->getAllgarantia();
            view("garantia/index.php", compact("garantiaCollection", "login"));
            exit;
        }
    }
}

<?php

namespace MyApp\Controllers {

    use MyApp\Models\User;
    use MyApp\Utils\Message;

    class UsersController
    {
        private $config = null;
        private $login = null;
        private $userModel = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index()
        {
            $login = $this->login;
            $message = new Message();

            $userModel = new User($this->config);
            $collection = $userModel->getAllUsers();
            view("users/index.php", compact("collection", "login"));
            exit;
        }

        // no se agrega el ->, compact("login") al view xq es la vista de registro nuevos usuarioss
        public function create()
        {
            $login = $this->login;
            view("users/create.php", compact("login"));
        }
        public function store()
        {
            $login = $this->login;
            $message = new Message();

            //var_dump($_POST);
            $fullname         = $_POST["fullname"];
            $username         = $_POST["username"];
            $password         = $_POST["password"];
            $confirm_password = $_POST["confirm_password"];
            $role             = $_POST["role"];
            var_dump($fullname, $username, $password, $role);

            $userModel = new User($this->config);
            $message = new Message();
            $result = $userModel->userExists($username);
            // verificar q todos los inputs esten llenos
            if ($fullname == "" || $username == "" || $password == "" || $confirm_password == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                view("users/create.php", compact("message", "login", "fullname", "username", "password", "confirm_password", "role"));
                exit;
            }
            if ($result["exists"] == 1) {
                $message->setWarningMessage(null, "Usuario ya existe", null, true);
                view("users/create.php", compact("message", "login", "fullname", "username", "password", "confirm_password", "role"));
                exit;
            }
            if ($password != $confirm_password) {
                $message->setWarningMessage(null, "Las contraseñas no coinciden", null, true);
                view("users/create.php", compact("message", "login", "fullname", "username", "password", "confirm_password", "role"));
                exit;
            }
            $userModel->insert($fullname, $username, $password, $role);
            $collection = $userModel->getAllUsers();
            view("users/index.php", compact("collection", "login"));
            exit;
        }

        public function update()
        {
            $login = $this->login;
            $message = new Message();

            $userId =  $_POST["edit_id"];
            $fullname         = $_POST["edit_fullname"];
            $username         = $_POST["edit_username"];
            $password         = $_POST["edit_pass"];
            $role             = $_POST["edit_role"];
            $blocked           = $_POST["edit_blocked"];

            $userModel = new User($this->config);

            if ($fullname == "" || $username == "" || $password == "") {
                $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
                exit;
            }

            $userModel->update($userId, $fullname, $username, $password, $role, $blocked);
            $collection = $userModel->getAllUsers();
            view("users/index.php", compact("collection", "login"));
            exit;
        }
        public function edit()
        {
            $login = $this->login;
            view("users/edit.php", compact("login"));
        }

        public function delete($id)
        {
            $login = $this->login;

            $userModel = new User($this->config);
            $userModel->delete($id);

            $collection = $userModel->getAllUsers();
            view("users/index.php", compact("collection", "login"));
            //view("users/index.php?", compact("collection", "login"));
            exit;
        }
    }
}

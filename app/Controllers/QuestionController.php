<?php

namespace MyApp\Controllers {

    use MyApp\Models\Questions;
    use MyApp\Utils\Message;

    class QuestionController
    {
        private $config = null;
        private $login = null;
        private $questionModel = null;

        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        public function index($id)
        {
            $login = $this->login;
            $message = new Message();

            $questionModel = new Questions($this->config);
            $questionCollection = $questionModel->get_Questionnaire_Questions($id);
            view("questions/index.php", compact("questionCollection", "login"));
            exit;
        }

        public function view()
        {
            $login = $this->login;
            $message = new Message();

            $questionModel = new Questions($this->config);
            $questionCollection = $questionModel->getAll();
            view("questions/index.php", compact("questionCollection", "login"));
            exit;
        }

        // no se agrega el ->, compact("login") al view xq es la vista de registro nuevos usuarioss
        public function create()
        {
            view("users/create.php");
        }
        public function store($id, $question)
        {
            $login = $this->login;
            $message = new Message();
            $questionModel = new Questions($this->config);


            // verificar q todos los inputs esten llenos
            if ($question == "") {
                $message->setWarningMessage(null, "El campo pregunta es requerido", null, true);
                $questionCollection = $questionModel->get_Questionnaire_Questions($id);
                view("questions/index.php", compact("questionCollection", "login"));
                exit;
            }
            $questionModel->insert($id, $question);
            $questionCollection = $questionModel->get_Questionnaire_Questions($id);
            view("questions/index.php", compact("questionCollection", "login"));
            exit;
        }
        public function update($questionaire, $question, $text)
        {
            $login = $this->login;

            $questionModel = new Questions($this->config);
            $message = new Message();

            //actualizar
            if ($text == "") {
                $message->setWarningMessage(null, "El campo pregunta es requerido", null, true);
                $questionCollection = $questionModel->get_Questionnaire_Questions($questionaire);
                view("questions/index.php", compact("questionCollection", "login"));
                exit;
            }
            $questionModel->update($question, $text);
            $questionCollection = $questionModel->get_Questionnaire_Questions($questionaire);
            view("questions/index.php", compact("questionCollection", "login"));
            exit;
        }
        public function delete($questionaire, $question)
        {
            $login = $this->login;
            $message = new Message();

            $questionModel = new Questions($this->config);
            $questionModel->delete($question);
            $questionCollection = $questionModel->get_Questionnaire_Questions($questionaire);
            view("questions/index.php", compact("questionCollection", "login"));
            exit;
        }
    }
}

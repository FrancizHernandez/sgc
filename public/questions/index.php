<?php
// Esta es el "dispatcher" de usuarios

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/QuestionController.php";
include_once MODELS . "/Questions.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\QuestionController;

$controller = new QuestionController($config, $login);

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'index':
            $controller->index($_GET['id']);
            break;
        case 'update':
            $controller->update($_GET['id'], $_GET['question'], $_GET['text']);
            break;
        case 'store':
            $controller->store($_GET['id'], $_GET['question']);
            break;
        case 'view':
            $controller->view();
            break;
        default:
        case 'delete':
            $controller->delete($_GET['id'], $_GET['question']);
            break;
    }
}

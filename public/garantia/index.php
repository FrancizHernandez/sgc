<?php
// Esta es el "dispatcher" de estados

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/GarantiaController.php";
include_once MODELS . "/Garantia.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\GarantiaController;

$controller = new GarantiaController($config, $login);

if (isset($_GET['action'])) {

    switch ($_GET['action']) {
        case 'index':
            $controller->index();
            break;
        case 'store':
            $controller->store($_GET['desc']);
            break;
        case 'update':
            $controller->update($_GET['id'], $_GET['desc']);
            break;
        case 'new':
            $controller->create();
            break;
    }
}

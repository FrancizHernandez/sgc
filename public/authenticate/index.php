<?php
// Esta es el "dispatcher" de usuarios

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/AuthController.php";
include_once MODELS . "/User.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\AuthController;

$controller = new AuthController($config, $login);

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'login':
            # pintar el login 
            $controller->login();
            break;
        case 'auth':
            $controller->auth();
            break;
        case 'register':
            $controller->register();
            break;
        case 'create':
            $controller->create();
            break;
        default:
        case 'logout':
            $controller->logout();
            break;
    }
}

<?php
// Esta es el "dispatcher" de usuarios

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/UsersController.php";
include_once MODELS . "/User.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\UsersController;

$controller = new UsersController($config, $login);


if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'create':
            $controller->create();
            break;
        case 'store':
            $controller->store();
            break;
        case 'index':
            $controller->index();
            break;
        case 'update':
            $controller->update();
            break;
        case 'delete':
            $controller->delete($_GET['id']);
            break;
        default:
            $controller->update();
            break;
    }
}

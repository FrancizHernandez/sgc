<?php
// Esta es el "dispatcher" de usuarios

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/AnswerController.php";
include_once MODELS . "/Answer.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\AnswerController;

$controller = new AnswerController($config, $login);

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'index':
            # pintar el login 
            $controller->index($_GET['id']);
            break;
        case 'store':
            $controller->store();
            break;
        case 'update':
            $controller->update();
            break;
        default:
        case 'delete':
            $controller->delete($_GET['id'], $_GET['preg']);
            break;
    }
}

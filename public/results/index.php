<?php
// Esta es el "dispatcher" de usuarios

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/ResultController.php";
include_once MODELS . "/Results.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\ResultController;

$controller = new ResultController($config, $login);

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'index':
            $controller->index($_GET['id']);
            break;
        case 'update':
            $controller->update($_POST);
            break;
        case 'store':
            $controller->store();
            break;
        case 'myResults':
            $controller->getResultsByUserAct();
            break;
        default:
        case 'delete':
            $controller->delete($_GET['id'], $_GET['quest']);
            break;
    }
}

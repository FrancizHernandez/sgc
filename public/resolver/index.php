<?php
// Esta es el "dispatcher" de usuarios

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/ResolverController.php";
include_once MODELS . "/Resolver.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\ResolverController;

$controller = new ResolverController($config, $login);

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'index':
            $controller->index($_GET['id']);
            break;
    }
}

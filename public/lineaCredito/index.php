<?php
// Esta es el "dispatcher" de estados

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/LineasCreditoController.php";
include_once MODELS . "/LineasCredito.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\LineasCreditoController;

$controller = new LineasCreditoController($config, $login);

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'index':
            $controller->index();
            break;
        case 'store':
            $controller->store($_GET['cod'], $_GET['desc'], $_GET['int']);
            break;
        case 'update':
            $controller->update($_GET['id'], $_GET['cod'], $_GET['desc'], $_GET['int']);
            break;
        case 'new':
            $controller->create();
            break;
    }
}

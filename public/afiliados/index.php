<?php
// Esta es el "dispatcher" de usuarios

// En cada script hay que cargar el init.php
include_once $_SERVER['DOCUMENT_ROOT'] . "/../app/init.php";

// Se cargan las clases que se ocupan
include_once CONTROLLERS . "/AfiliadoController.php";
include_once MODELS . "/Afiliado.php";

// Se hace el "use" de las clases que se utilizaran en este script
use MyApp\Controllers\AfiliadoController;

$controller = new AfiliadoController($config, $login);
if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'index':
            # pintar el login 
            $controller->index();
            break;
        case 'update':
            $controller->update($_GET['id'], $_GET['desc'], $_GET['long_desc']);
            break;
        case 'store':
            $controller->store($_GET['description'], $_GET['long_description']);
            break;
        default:
        case 'delete':
            $controller->delete($_GET['id']);
            break;
        case 'create':
            $controller->create();
            break;
    }
}
